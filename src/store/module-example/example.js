import axios from "axios";
const state = {
    dataTable: [
        {
            id: 1,
            name: 'Go to shop',
            completed: false,
            dueDate: '2019/05/12',
            dueTime: '18.30'
        },
        {
            id: 2,
            name: 'Go to s',
            completed: true,
            dueDate: '2019/05/12',
            dueTime: '18.30'
        },
        
        {
            id: 3,
            name: 'Go to AAAA',
            completed: true,
            dueDate: '2019/05/12',
            dueTime: '18.30'
        }
    ],
    setDat: []
}
const mutations = {
    updateData(state, payload){
        state.setDat = payload;
    }
}
const actions = {
    updateData({commit}, payload) {
        axios.get('http://demo2725834.mockable.io/api/v1/list').then((response) => {
        response.data.data.forEach((item, i) => {
            item.id = i + 1;
          });
        commit('updateData', response.data.data)
      });
    }
}
const getters = {
    dataTable: (state)=>{
        return state.dataTable
    },
    setDat: (state)=>{
        return state.setDat
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}