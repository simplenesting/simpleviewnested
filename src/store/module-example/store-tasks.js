const state = {
    tasks: [
        {
            id: 1,
            name: 'Go to shop',
            completed: false,
            dueDate: '2019/05/12',
            dueTime: '18.30'
        },
        {
            id: 2,
            name: 'Go to s',
            completed: true,
            dueDate: '2019/05/12',
            dueTime: '18.30'
        },
        
        {
            id: 3,
            name: 'Go to AAAA',
            completed: true,
            dueDate: '2019/05/12',
            dueTime: '18.30'
        }
    ]
}
const mutations = {
    updateTask(state, payload){
        for (var i in state.tasks) {
            if (state.tasks[i].id == payload.id) {
                state.tasks[i].completed = payload.completed;
               break; //Stop this loop, we found it!
            }
          }
    }
}
const actions = {
    updateTask({commit}, payload) {
        commit('updateTask', payload)
    }
}
const getters = {
    tasks: (state)=>{
        return state.tasks
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}